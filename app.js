const express = require('express')
const bodyParser = require('body-parser');
const res = require('express/lib/response');
const app = express ();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/clientes/filtros',(req,res)=>{
    let source = req.query;
    let ret = "Dados enviados:" + source.nome + " " + source.sobrenome;
    res.send("{mensagem:"+ret+"}");
});
app.post('/clientes', (req,res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log ("Valor Access: " + headers_);
    if(headers_ == '123456'){
        res.send("Acesso de cliente autorizado")
    }else{
        res.send("Cliente negado")
    }
});

app.get('/funcionarios', (req,res)=>{
    res.send("{message: Funcionario encontrado}");
});

app.delete('/funcionarios/:valor', (req, res)=>{
    let dados = req.params.valor;
    let headers_ = req.headers["access"];
    let msg;
    if(headers_ == 123456){
        msg = 'acesso concedido';
        res.send("{menssage: " + dados +" "+msg+ "}");
    }else{
        msg = 'acesso negado';
        res.send("{message: " +msg+"}");
    }
    });